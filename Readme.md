# Cabify Code Challengue

Create the image from Dockerfile

`docker build -t cabify_challengue_ruby .`

You can use next script to run tests:
`./bin/run-tests.sh`

You can use the library as follows:

```ruby
ch = Checkout.new
ch.scan("VOUCHER")
ch.scan("TSHIRT")
ch.scan("VOUCHER")
ch.scan("VOUCHER")
ch.scan("MUG")
ch.scan("TSHIRT")
ch.scan("TSHIRT")

puts ch.total
```

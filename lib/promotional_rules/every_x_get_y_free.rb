require_relative './min_num_items'

class EveryXGetYFree < MinNumItems
  def apply(items)
    items_free = (items.length / @min_num_items) * @value
    total_price = items.length * items.first['price']
    discount = items.first['price'] * items_free
    (total_price - discount) / items.length
  end
end

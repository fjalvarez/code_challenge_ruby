require_relative './every_x_get_y_free'
require_relative './product_price_bulk'

class Factory
  def self.build(type, *args)
    case type
    when "EveryXGetYFree"
      return EveryXGetYFree.new(*args)
    when "ProductPriceBulk"
      return ProductPriceBulk.new(*args)
    end
  end
end

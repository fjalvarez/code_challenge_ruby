class MinNumItems
  attr_accessor :min_num_items, :value

  def initialize(min_num_items, value)
    @min_num_items = min_num_items
    @value = value
  end

  def applies(items)
    items.length >= @min_num_items
  end
end

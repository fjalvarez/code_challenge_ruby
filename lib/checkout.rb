require_relative './promotional_rules/factory'
require 'yaml'

class Checkout

  # initialize : checkout constructor
  def initialize(path = "config/dataset.yml")
    @cart = {}
    @path = path
    @inventory = nil
    @promotions = nil
  end

  # scan : adds a product to the cart by code
  def scan(code)
    return if not inventory.key? code
    @cart[code] = [] if @cart[code].nil?
    @cart[code] << inventory[code]
  end

  # total : calculates the final amount for the current cart
  def total
    return 0 if @cart.empty?
    total_price = 0
    @cart.each do |code, list|
     total_price += price_per_unit(code, list) * list.length
    end
    total_price
  end

  private

  def price_per_unit(code, list)
    if not promotions.key? code
      return list.first['price']
    end
    @promotions[code].each do |name, promotion|
      if promotion.applies(list)
        return promotion.apply(list)
      end
    end
    list.first['price']
  end

  # inventory : retrieves the inventory
  def inventory
    return @inventory if not @inventory.nil?
    @inventory = YAML.load_file(@path)["products"]
    @inventory
  end

  # promotions : retrieves the promotions
  def promotions
    return @promotions if not @promotions.nil?
    @promotions = YAML.load_file(@path)["promotions"]
    @promotions.each do |code, promotions|
      promotions.each do |type, promotion|
        @promotions[code][type] = Factory.build(type, promotion['min_num_items'], promotion['value'])
      end
    end
    @promotions
  end
end

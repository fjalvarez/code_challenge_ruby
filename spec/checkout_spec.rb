require_relative '../lib/checkout'

describe Checkout do
  before do
     @checkout = Checkout.new("./spec/fixtures/dataset.yml")
     @cabify_cap = {"name"=>"Cabify Cap", "price"=>10.0}
   end

  describe '#scan' do
      context 'given an empty code' do
        it 'returns nil' do
          expect(@checkout.scan("")).to eq nil
        end
      end
      context 'not existing element' do
        it 'returns nil' do
          expect(@checkout.scan("CLOCK")).to eq nil
        end
      end
      context 'valid element' do
        it 'returns the element' do
          expect(@checkout.scan("CAP")).to include @cabify_cap
        end
      end
  end

  describe '#total' do
    context 'first example' do
      before do
        @checkout.scan('SNEAKERS')
        @checkout.scan('CAP')
        @checkout.scan('PEN')
      end

      it 'returns total price' do
        expect(@checkout.total).to eq 110.5
      end
    end

    context 'example 2' do
      before do
        @checkout.scan('SNEAKERS')
        @checkout.scan('SNEAKERS')
      end

      it 'returns total price' do
        expect(@checkout.total).to eq 100.0
      end
    end

    context 'example 3' do
      before do
        @checkout.scan('CAP')
        @checkout.scan('CAP')
      end

      it 'returns total price' do
        expect(@checkout.total).to eq 20.0
      end
    end

    context 'example 4' do
      before do
        @checkout.scan('CAP')
        @checkout.scan('CAP')
        @checkout.scan('CAP')
      end

      it 'returns total price' do
        expect(@checkout.total).to eq 22.5
      end
    end


    context 'example 5' do
      before do
        @checkout.scan('CAP')
        @checkout.scan('CAP')
        @checkout.scan('CAP')
        @checkout.scan('CAP')
        @checkout.scan('SNEAKERS')
        @checkout.scan('SNEAKERS')
        @checkout.scan('PEN')
        @checkout.scan('PEN')
      end

      it 'returns total price' do
        expect(@checkout.total).to eq 131.0
      end
    end
  end
end

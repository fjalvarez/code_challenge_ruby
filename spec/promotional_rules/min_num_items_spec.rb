require_relative '../../lib/promotional_rules/min_num_items'

describe MinNumItems do
  describe '#applies' do
      before(:all) do
        @rule = MinNumItems.new(2, 1)
      end
      it 'doesnt applies if the number if lower' do
        expect(@rule.applies([])).to be_falsy
      end
      it 'applies if the number if greater' do
        expect(@rule.applies([1, 2, 3])).to be_truthy
      end
  end
end

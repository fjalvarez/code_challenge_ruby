require_relative '../../lib/promotional_rules/product_price_bulk'

describe ProductPriceBulk do
  describe '#apply' do
      before(:all) do
        @buy_x_elements = 2
        @new_price = 5.0
        @rule = ProductPriceBulk.new(@buy_x_elements, @new_price)
      end
      it 'calculate the price' do
        num_items = 4
        price = 10.0

        list = []
        num_items.times do
          list << {"name"=>"Cabify Cap", "price"=>price}
        end
        expect(@rule.apply(list)).to eq(@new_price)
      end
  end
end

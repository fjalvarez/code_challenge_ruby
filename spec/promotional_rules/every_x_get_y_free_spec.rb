require_relative '../../lib/promotional_rules/every_x_get_y_free'

describe EveryXGetYFree do
  describe '#apply' do
      before(:all) do
        @buy_x_elements = 2
        @get_y_free_elements = 1
        @rule = EveryXGetYFree.new(@buy_x_elements, @get_y_free_elements)
      end
      it 'calculate the price' do
        num_items = 4
        price = 10.0

        list = []
        num_items.times do
          list << {"name"=>"Cabify Cap", "price"=>price}
        end
        expect(@rule.apply(list)).to eq(price / @buy_x_elements)
      end
  end
end

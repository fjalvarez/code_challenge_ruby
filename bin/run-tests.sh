#!/usr/bin/env bash
DIR=$( cd "$( dirname "$0" )" && cd .. && pwd )

cd $DIR

$DIR/bin/docker-runner.sh bundle exec rspec -f d
